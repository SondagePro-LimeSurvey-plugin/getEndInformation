getEndInformation
==================

A plugin for LimeSUrvey fo fill a question with quota and/or end url information.

## Installation

### Via GIT
- Go to your LimeSurvey Directory (version up to 2.06 only)
- Clone in plugins/getEndInformation directory

##Home page & Copyright
- HomePage <http://wwww.sondages.pro/>
- Copyright © 2015 Denis Chenu <http://sondages.pro>
- Copyright © 2015 2015 Ysthad <http://ysthad.fr>

Distributed under [GNU GENERAL PUBLIC LICENSE Version 3](http://www.gnu.org/licenses/gpl.txt) licence
