<?php
/**
 * getEndInformation Plugin for LimeSurvey
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2015 Denis Chenu <http://sondages.pro>
 * @copyright 2015 Ysthad <http://ysthad.fr>
 * @license GPL v3
 * @version 0.9
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */
class getEndInformation extends \ls\pluginmanager\PluginBase
{
    protected $storage = 'DbStorage';

    static protected $name = 'getEndInformation';
    static protected $description = 'Fill a question with quota or end url information';

    protected $settings = array(
        'getQuotaIdQuestion' => array(
            'type' => 'string',
            'label' => 'The question code to be filled with the quota id.',
            'default' => 'getQuotaId'
        ),
        'getQuotaNameQuestion' => array(
            'type' => 'string',
            'label' => 'The question code to be filled with the quota name.',
            'default' => 'getQuotaName'
        ),
        'getQuotaNameEmQuestion' => array(
            'type' => 'string',
            'label' => 'The question code to be filled with the quota name and Expression Manager.',
            'default' => 'getQuotaNameEm'
        ),
        'getQuotaUrlQuestion' => array(
            'type' => 'string',
            'label' => 'The question code to be filled with the quota url (not updated).',
            'default' => 'getQuotaUrl'
        ),
        'getQuotaUrlEmQuestion' => array(
            'type' => 'string',
            'label' => 'The question code to be filled with the quota url (updated by Expression Manager).',
            'default' => 'getQuotaUrlEm'
        ),
        'getQuotaUrlDescQuestion' => array(
            'type' => 'string',
            'label' => 'The question code to be filled with the quota url description (not updated).',
            'default' => 'getQuotaUrlDesc'
        ),
        'getQuotaUrlDescEmQuestion' => array(
            'type' => 'string',
            'label' => 'The question code to be filled with the quota url description (updated by Expression Manager).',
            'default' => 'getQuotaUrlDescEm'
        ),
        'defaultUrlRedirect' => array(
            'type'=>'select',
            'label' => 'If completed url is set (and this plugin have question for end url) : redirect by default, this is needed if you need redirect and completed url information.',
            'options'=>array(
                "Y"=>"Yes",
                "N"=>"No",
            ),
            'default'=>"Y",
        ),
        'getUrlQuestion' => array(
            'type' => 'string',
            'label' => 'The question code to be filled with the end url (not updated).',
            'default' => 'getEndUrl'
        ),
        'getUrlEmQuestion' => array(
            'type' => 'string',
            'label' => 'The question code to be filled with the end url (updated by Expression Manager).',
            'default' => 'getEndUrlEm'
        ),
        'getUrlDescQuestion' => array(
            'type' => 'string',
            'label' => 'The question code to be filled with the end url description (not updated).',
            'default' => 'getEndUrlDesc'
        ),
        'getUrlDescEmQuestion' => array(
            'type' => 'string',
            'label' => 'The question code to be filled with the end url description (updated by Expression Manager).',
            'default' => 'getEndUrlDescEm'
        ),
    );

    private $aQuestionsCode=array(
              'getQuotaIdQuestion'=>array(
                'valueForQuota'=>'id',
              ),
              'getQuotaNameQuestion'=>array(
                'valueForQuota'=>'name',
              ),
              'getQuotaNameEmQuestion'=>array(
                'em'=>true,
                'valueForQuota'=>'name',
              ),
              'getQuotaUrlQuestion'=>array(
                'valueForQuota'=>'quotals_url',
                'isUrl'=> true,
              ),
              'getQuotaUrlEmQuestion'=>array(
                'em'=>true,
                'valueForQuota'=>'quotals_url',
                'isUrl'=> true,
              ),
              'getQuotaUrlDescQuestion'=>array(
                'valueForQuota'=>'quotals_urldescrip',
              ),
              'getQuotaUrlDescEmQuestion'=>array(
                'em'=>true,
                'valueForQuota'=>'quotals_urldescrip',
              ),
              'getUrlQuestion'=>array(
                'valueForQuota'=>'quotals_url',
                'valueForEnd'=>'surveyls_url',
                'isUrl'=> true,
              ),
              'getUrlEmQuestion'=>array(
                'em'=>true,
                'valueForQuota'=>'quotals_url',
                'valueForEnd'=>'surveyls_url',
                'isUrl'=> true,
              ),
              'getUrlDescQuestion'=>array(
                'valueForEnd'=>'surveyls_urldescription',
                'valueForQuota'=>'quotals_urldescrip',
              ),
              'getUrlDescEmQuestion'=>array(
                'em'=>true,
                'valueForEnd'=>'surveyls_urldescription',
                'valueForQuota'=>'quotals_urldescrip',
              ),
    );
    public function init() {
        $this->subscribe('afterSurveyComplete');
        $this->subscribe('afterSurveyQuota');
        $this->subscribe('beforeSurveySettings');
        $this->subscribe('newSurveySettings');
    }

    public function beforeSurveySettings()
    {
        $event = $this->event;
        $sDefaultRedirect=$this->get('defaultUrlRedirect',null,null,$this->settings['defaultUrlRedirect']['default']);

        $event->set("surveysettings.{$this->id}", array(
            'name' => get_class($this),
            'settings' => array(
                'active'=>array(
                    'type'=>'select',
                    'label'=>'Use getQuestionOrder infor plugin',
                    'options'=>array(
                        '0'=>gt("No"),
                        '1'=>gt("Yes"),
                    ),
                    'current' => $this->get('active', 'Survey', $event->get('survey'),1),
                ),
                'surveyUrlRedirect'=>array(
                    'type'=>'select',
                    'label'=>'Redirect url (if one question is set)',
                    'options'=>array(
                        'default'=>sprintf(gt("Leave default (%s)",'unescaped'),$sDefaultRedirect=="Y" ? gt("Yes") : gt("No")),
                        'N'=>gt("No"),
                        'Y'=>gt("Yes"),
                    ),
                    'current' => $this->get('surveyUrlRedirect', 'Survey', $event->get('survey'),'default'),
                    'help' => sprintf("Actual default option is : %s",$sDefaultRedirect=="Y" ? gt("Yes") : gt("No")),
                ),
            )
        ));
    }
    public function newSurveySettings()
    {
        $event = $this->event;
        foreach ($event->get('settings') as $name => $value)
        {
            /* In order use survey setting, if not set, use global, if not set use default */
            $default=$event->get($name,null,null,isset($this->settings[$name]['default'])?$this->settings[$name]['default']:NULL);
            $this->set($name, $value, 'Survey', $event->get('survey'),$default);
        }
    }

    public function afterSurveyComplete()
    {
        $oEvent=$this->getEvent();
        $iSurveyId=$oEvent->get('surveyId');

        $oSurvey=Survey::model()->findByPk($iSurveyId);
        if($oSurvey && $this->get('active', 'Survey', $iSurveyId,1))
        {
            // $redata for templatereplace
            $aSurveyInfo=getSurveyInfo($iSurveyId, $_SESSION['survey_'.$iSurveyId]['s_lang']);
            $sTemplatePath=getTemplatePath($aSurveyInfo['template']);
            $sClientToken=isset($_SESSION['survey_'.$iSurveyId]['token'])?$_SESSION['survey_'.$iSurveyId]['token']:"";
            $aDataReplacement = array(
                'thissurvey'=>$aSurveyInfo,
                'clienttoken'=>$sClientToken,
                'token'=>$sClientToken,
            );
            $aUpdateFields=array();
            foreach($this->aQuestionsCode as $sSetting=>$aQuestion)
            {
                $aQuestion['qCode']=$this->get($sSetting,null,null,$this->settings[$sSetting]['default']);
                if($aQuestion['qCode'] && isset($aQuestion['valueForEnd']))
                {
                    $aQuestion=array_merge(array('em'=>false,'isUrl'=>false),$aQuestion);
                    $oQuestion=Question::model()->find("sid=:sid and title=:title and parent_qid=0",array(":sid"=>$iSurveyId,":title"=>$aQuestion['qCode']));
                    if($oQuestion)
                    {
                        $value=$aSurveyInfo[$aQuestion['valueForEnd']];
                        if($aQuestion['em'])
                        {
                          if($aQuestion['isUrl'])
                            $value=passthruReplace($value, $aSurveyInfo);
                          $value=templatereplace($value,array(),$aDataReplacement, '', $aSurveyInfo['anonymized']!='N', NULL, array(), true );
                        }
                        $aUpdateFields["{$oQuestion->sid}X{$oQuestion->gid}X{$oQuestion->qid}"]=$value;
                    }
                }
            }
            if(count($aUpdateFields))
            {
              $this->updateActualResponse($aUpdateFields);
              $sRedirectParams=$this->get('surveyUrlRedirect', 'Survey', $iSurveyId,'default');
              if($sRedirectParams=="default")
                $sRedirectParams=$this->get('defaultUrlRedirect', null, null,$this->settings['defaultUrlRedirect']['default']);
              if($sRedirectParams=="Y")
              {
                $sLocation=passthruReplace($aSurveyInfo['surveyls_url'], $aSurveyInfo);
                $sLocation=templatereplace($sLocation,array(),$aDataReplacement, '', $aSurveyInfo['anonymized']!='N', NULL, array(), true );
                //Update the token if needed and send a confirmation email
                if (isset($_SESSION['survey_'.$iSurveyId]['token']))
                {
                    submittokens();
                }
                sendSubmitNotifications($iSurveyId);
                // Did we close session ?
                // The session cannot be killed until the page is completely rendered
                if ($aSurveyInfo['printanswers'] != 'Y')
                {
                    killSurveySession($iSurveyId);
                }
                header("Location: {$sLocation}");
              }
            }
        }
    }
    public function afterSurveyQuota()
    {
        $oEvent=$this->getEvent();
        $iSurveyId=$oEvent->get('surveyId');

        $oSurvey=Survey::model()->findByPk($iSurveyId);
        if($oSurvey && $this->get('active', 'Survey', $iSurveyId,1))
        {
            $aMatchedQuotas=$oEvent->get('aMatchedQuotas');
            $aMatchedQuota=$aMatchedQuotas[0];
            // $redata for templatereplace
            $aSurveyInfo=getSurveyInfo($iSurveyId, $_SESSION['survey_'.$iSurveyId]['s_lang']);
            $sTemplatePath=getTemplatePath($aSurveyInfo['template']);
            $sClientToken=isset($_SESSION['survey_'.$iSurveyId]['token'])?$_SESSION['survey_'.$iSurveyId]['token']:"";
            $aDataReplacement = array(
                'thissurvey'=>$aSurveyInfo,
                'clienttoken'=>$sClientToken,
                'token'=>$sClientToken,
            );

            $aUpdateFields=array();
            foreach($this->aQuestionsCode as $sSetting=>$aQuestion)
            {
              $aQuestion['qCode']=$this->get($sSetting,null,null,$this->settings[$sSetting]['default']);
              if($aQuestion['qCode'])
              {
                $aQuestion=array_merge(array('em'=>false,'isUrl'=>false),$aQuestion);
                $oQuestion=Question::model()->find("sid=:sid and title=:title and parent_qid=0",array(":sid"=>$iSurveyId,":title"=>$aQuestion['qCode']));
                if($oQuestion)
                {
                  $value=$aMatchedQuota[$aQuestion['valueForQuota']];
                  if($aQuestion['em'])
                  {
                    if($aQuestion['isUrl'])
                      $value=passthruReplace($value, $aSurveyInfo);
                    $value=templatereplace($value,array(),$aDataReplacement, '', $aSurveyInfo['anonymized']!='N', NULL, array(), true );
                  }
                  $aUpdateFields["{$oQuestion->sid}X{$oQuestion->gid}X{$oQuestion->qid}"]=$value;
                }
              }
            }
            if(count($aUpdateFields))
              $this->updateActualResponse($aUpdateFields);
        }
    }

    /**
    * Update a response by array of value and key for attribute
    * @param integer $iResponseId
    * @param array $aUpdatedFields
    * @return void
    **/
    private function updateActualResponse($aUpdatedFields)
    {
      if(count($aUpdatedFields)===0)
        return;
      $iSurveyId=$this->getEvent()->get('surveyId');
      $oSurvey=Survey::model()->findByPk($iSurveyId);
      if($oSurvey->active!="Y")
        return;
      $iResponseId=$this->getEvent()->get('responseId');
      $oReponse=Response::model($this->getEvent()->get('surveyId'))->find("id=:responseid",array(":responseid"=>$iResponseId));// findByPk ?
      if($oReponse)
      {
        foreach($aUpdatedFields as $key=>$value)
          $oReponse->$key=$value;
        if(!$oReponse->save())
          tracevar($oReponse->getErrors());
      }
    }
}
